const express =  require ('express')
const cors = require ('cors')

const callCenterProxy = require ('./proxy/callcenter-api.proxy')
const userProxy = require ('./proxy/user-api.proxy')

const app =  express()

app.use( cors() )
app.use( express() )

userProxy(app)
callCenterProxy(app)

app.listen(8080, () => {
    console.log('Gateway is up!')
})