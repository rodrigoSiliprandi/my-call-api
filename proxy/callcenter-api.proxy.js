const httpProxy = require ('express-http-proxy')
const callCenterProxy = httpProxy ("http://localhost:8082")

module.exports = (app) => {
    app.get('/call', (req, res, next) => {
        callCenterProxy(req, res, next)
    })

    app.post('/call', (req, res,next) => {
        callCenterProxy(req, res, next)
    })
}