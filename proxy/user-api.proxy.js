const httpProxy = require('express-http-proxy')
const userProxy = httpProxy("http://localhost:8081")

module.exports = (app) => {
    app.post('user/login', (req, res, next) => {
        userProxy(req, res, next)
    })
}